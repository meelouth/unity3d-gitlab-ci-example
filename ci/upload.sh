#!/usr/bin/env bash

set -x

echo "Upload to Amazon"

aws s3 cp ./Builds/Android/${BUILD_NAME}.apk s3://unity-game/Android/${BUILD_NAME}.apk  --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --region eu-central-1

echo "<html><title>Download apk: ${AppBundle}</title><body><a href=\"https://unity-game.s3.eu-central-1.amazonaws.com/Android/KimonoBuild.apk\">Install<br><br></a></body></html>" >> ./Builds/Android/download.html

aws s3 cp ./Builds/Android/download.html s3://unity-game/Android/download.html  --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --region eu-central-1

ls

chmod +x ./ci/notify.sh && ./ci/notify.sh ✅ https://unity-game.s3.eu-central-1.amazonaws.com/Android/download.html